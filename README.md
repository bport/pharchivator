# Pharchivator

Goal of this project is to create a static archive of phabricator

## Usage

### Installation

Project depends on python 3.8 and as 3 main dependencies 
- requests
- lxml
- beautifulsoup4

### Fair use
Scrap data only when necessary, don't scrap them at each run!!!

```
# first run
./main.py --scrap --transform /phab/input /phab/output

# following run (unless you did some change on scraping code
./main.py --transform /phab/input /phab/output
```

## Page scrapped

- task (for static task page generation)
- diff (for static diff page generation)
- diffusion list (for repository mapping Rxxx -> repository name)

## Which adaptations are done
### Global

- Remove all interactions actions
    - login
    - search
    -edit
    - ...
- Redirect links to gitlab
    - commit
    - repository
    - users
- Scrap images
    - TODO scrap background-images (like avatar)
- Scrap css
- TODO handle tag (delete link and don't scrap project page, or scrap project page)
    
### Code review / differential

- actual diff are loaded directly not through an AJAX call (case on phabricator)
- Don't allow to navigate through context
- TODO
    - commit and history tabs
    
### Tasks

- Almost working
- TODO
    - mentions tabs

### Other area

TODO

