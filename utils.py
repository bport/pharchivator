# SPDX-FileCopyrightText: 2020 Benjamin Port <benjamin.port@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later


def remove_all(soup, *args, **kwargs):
    for item in soup.find_all(*args, **kwargs):
            item.decompose()
