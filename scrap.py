# SPDX-FileCopyrightText: 2020 Benjamin Port <benjamin.port@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
import json
import os
import re
import time
from functools import lru_cache
from urllib.parse import urlparse

import requests

from bs4 import BeautifulSoup

from utils import remove_all

class RateLimitedRequest():
    def __init__(self, rate_limit):
        self.last_call = None
        self.session = requests.session()
        self.session.headers = {'User-Agent': 'Pharchivator/0.1'}
        self.rate_limit = rate_limit

    def __getattr__(self, attr):
        if attr == "get":
            return self.proxy_get
        elif attr == "post":
            return self.proxy_post
        else:
            raise AttributeError()

    def wait_until_ready(self):
        """
        Wait at least "rate limit" seconds between each api call
        """
        current_time = time.monotonic()
        waiting_time = self.rate_limit - (current_time - self.last_call) if self.last_call else 0
        if waiting_time > 0:
            time.sleep(waiting_time)

    def proxy_get(self, *args, **kwargs):
        self.wait_until_ready()
        print("get")
        response = self.session.get(*args, **kwargs)
        self.last_call = time.monotonic()
        return response

    def proxy_post(self, *args, **kwargs):
        self.wait_until_ready()
        response = self.session.post(*args, **kwargs)
        self.last_call = time.monotonic()
        return response


r_sess = RateLimitedRequest(1)


# Use lru_cache to prevent downloading twice same resource
@lru_cache(maxsize=None)
def scrap_resource(url, dest_dir):
    dir_name = os.path.dirname(urlparse(url).path[1:])
    file_name = os.path.basename(urlparse(url).path[1:])
    absolute_dir_path = os.path.join(dest_dir, dir_name)
    absolute_file_path = os.path.join(absolute_dir_path, file_name)
    os.makedirs(absolute_dir_path, exist_ok=True)
    response = r_sess.get(url)
    with open(absolute_file_path, "wb") as file:
        file.write(response.content)


def scrap_img(soup, input_dir):
    # scrap images
    for img in soup.find_all("img"):
        scrap_resource(img["src"], input_dir)
        img["src"] = urlparse(img["src"]).path[1:]
        image_link = img.find_parent("a")
        scrap_resource(image_link.get("href"), input_dir)
        image_link["href"] = urlparse(image_link["href"]).path[1:]


def scrap_link(soup, input_dir):
    for link in soup.find_all("link"):
        # TODO filter thing we need to keep
        url = link.get("href")
        scrap_resource(url, input_dir)
        link["href"] = urlparse(url).path[1:]


def scrap_diff(input_dir):
    changeset_re = re.compile(r"\/differential\/changeset\/\?ref=(?P<ref>\d*)")

    for i in range(9999, 10002):
        diff_ref = f"D{i}"
        result = r_sess.get(f"https://phabricator.kde.org/{diff_ref}")
        if result.status_code != 200:
            continue

        # Real diff are loaded through an ajax call, we load them directly
        # build changeset mapping
        soup = BeautifulSoup(result.content, "lxml")
        changeset_mapping = {}
        for data in soup.find_all("data"):
            for item in json.loads(data.get("data-javelin-init-data")).get("data", []):
                if "containerID" in item and "standaloneURI" in item:
                    if m:= changeset_re.match(item["standaloneURI"]):
                        ref = m.group("ref")
                        changeset_mapping[item["containerID"]] = ref

        # integrate diff
        for changeset in soup.find_all("div", class_="differential-loading"):
            changeset_id = changeset.find_parent("div", class_="differential-changeset").get("id")
            changeset_ref = changeset_mapping[changeset_id]
            data = f"ref={changeset_ref}&whitespace=ignore-most&renderer=2up&highlight=&encoding=&__wflow__=true&__ajax__=true&__metablock__=2"
            response = r_sess.post("https://phabricator.kde.org/differential/changeset/", data=data)
            changeset_content = json.loads(response.content[9:])["payload"]["changeset"]
            changeset.replace_with(BeautifulSoup(changeset_content, "html.parser"))

        # remove all scripts
        remove_all(soup, "script")

        scrap_img(soup, input_dir)
        scrap_link(soup, input_dir)

        # save to input folder
        with open(os.path.join(input_dir, diff_ref), "w") as file:
            file.write(str(soup))

        print(f"{diff_ref} scraping done")


def scrap_tasks(input_dir):
    for i in range(9999, 10002):
        task_ref = f"T{i}"
        result = r_sess.get(f"https://phabricator.kde.org/{task_ref}")
        if result.status_code != 200:
            continue

        soup = BeautifulSoup(result.content, "lxml")

        # remove all scripts
        remove_all(soup, "script")

        scrap_img(soup, input_dir)
        scrap_link(soup, input_dir)

        # save to input folder
        with open(os.path.join(input_dir, task_ref), "w") as file:
            file.write(str(soup))

        print(f"{task_ref} scraping done")


def build_repository_dict(input_dir):
    repo_dict = {}

    def process_diffusion_page(url):
        response = r_sess.get(url)
        soup = BeautifulSoup(response.content, "lxml")
        for repo in soup.find_all("span", attrs={"data-sigil": "slippery"}):
            repo_key = repo.find("span", class_="phui-oi-objname").text
            repo_name = repo.find("a", class_="phui-oi-link")["href"].split("/")[-2]
            repo_dict[repo_key] = repo_name
        # get next tag
        next_tag = soup.find("div", class_="phui-button-text", text="Next")
        if next_tag:
            next_url = next_tag.parent["href"]
            process_diffusion_page(f"https://phabricator.kde.org{next_url}")

    process_diffusion_page("https://phabricator.kde.org/diffusion/")

    with open(os.path.join(input_dir, "repo_dict.json"), "w") as file:
        json.dump(repo_dict, file, indent=4)


def scrap_all(input_dir):
    scrap_diff(input_dir)
    scrap_tasks(input_dir)
    build_repository_dict(input_dir)
