# SPDX-FileCopyrightText: 2020 Benjamin Port <benjamin.port@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
import json
import os
import re
import shutil
from os.path import isdir

from utils import remove_all

commit_re = re.compile(r"\/(?P<repo_id>R[0-9]+):(?P<commit_id>[\da-z]+)")

from bs4 import BeautifulSoup

INVENT_URL = "https://invent.kde.org/"


def transform_urls(soup, repo_mapping):
    urls = soup.find_all("a", href=True)
    for url in urls:
        path = url.get("href", None)
        if path:
            if path.startswith("/p/"): # people
                url["href"] = INVENT_URL + path.split("/")[-2]
            elif path.startswith("/source/"):
                url["href"] = INVENT_URL + "/kde/" + path.split("/")[-2]
            elif m := commit_re.match(path):
                repo_name = repo_mapping[m.group("repo_id")]
                repo_url = INVENT_URL + "/kde/" + repo_name + "/"
                url["href"] = repo_url + "-/commit/" + (m.group("commit_id"))
            else:
                pass


def common_clean_up(soup, repo_mapping):
    # Remove search
    remove_all(soup, "ul", class_="phabricator-search-menu")
    # remove login button
    remove_all(soup, "a", class_="phabricator-core-login-button")
    # remove breadcrumb
    remove_all(soup, "div", class_="phui-crumbs-view")
    # remove policy header
    remove_all(soup, "span", class_="policy-header-callout")
    # remove comment box (login at bottom)
    soup.find("a", "login-to-comment").parent.decompose()
    # remove action list (right panel action)
    remove_all(soup, "ul", "phabricator-action-list-view")
    # remove timeline menu
    remove_all(soup, "a", "phui-timeline-menu")
    # transform urls to map gitlab one
    transform_urls(soup, repo_mapping)


def transform_diff(soup, repo_mapping):
    common_clean_up(soup, repo_mapping)

    # replace show diff context to no context available
    for context_target in soup.find_all("tr", attrs={"data-sigil": "context-target"}):
        context_target.replace_with(BeautifulSoup("""<tr data-sigil="context-target"><td colspan="6" class="show-more">Context not available.</td></tr>""", "html.parser"))

    # remove reply button to inline comment
    remove_all(soup, "a", attrs={"data-sigil": "has-tooltip differential-inline-reply"})
    # remove view options button
    remove_all(soup, "a", attrs={"data-sigil": "differential-view-options"})
    # remove all header action links
    remove_all(soup, "div", class_="phui-header-action-links")


def transform_task(soup, repo_mapping):
    common_clean_up(soup, repo_mapping)


def transform_all(input_dir, output_dir):
    with open(os.path.join(input_dir, "repo_dict.json")) as file:
        repo_mapping = json.load(file)

    for filename in os.listdir(input_dir):
        absolute_input_path = os.path.join(input_dir, filename)
        absolute_output_path = os.path.join(output_dir, filename)

        if isdir(absolute_input_path):
            shutil.copytree(absolute_input_path, absolute_output_path, dirs_exist_ok=True)
        if filename.startswith("D"):
            with open(absolute_input_path, "r") as file:
                contents = file.read()
            soup = BeautifulSoup(contents, "lxml")
            transform_diff(soup, repo_mapping)
            with open(absolute_output_path, "w") as file:
                file.write(str(soup))
        if filename.startswith("T"):
            with open(absolute_input_path, "r") as file:
                contents = file.read()
            soup = BeautifulSoup(contents, "lxml")
            transform_task(soup, repo_mapping)
            with open(absolute_output_path, "w") as file:
                file.write(str(soup))

