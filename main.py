#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020 Benjamin Port <benjamin.port@kde.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

import argparse
import scrap
import transform

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--scrap", action="store_true", help="scrap website")
    parser.add_argument("--transform", action="store_true", help="make a static version from scraped data")
    parser.add_argument("input", help="Dir used to save scraped file")
    parser.add_argument("output", help="Dir used to output transformed file")
    args = parser.parse_args()

    if args.scrap:
        scrap.scrap_all(args.input)

    if args.transform:
        transform.transform_all(args.input, args.output)
